# Generated by Django 4.1.7 on 2023-04-03 07:51

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('players', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='player',
            options={'verbose_name': 'Гравець', 'verbose_name_plural': 'Гравці'},
        ),
    ]
