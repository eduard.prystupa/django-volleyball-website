from django.db import models

class Player(models.Model):
    name = models.CharField("Ім'я", max_length=50, default='')
    second_name = models.CharField("Прізвище", max_length=50, default='')
    photo = models.ImageField('Photo', upload_to='players_photos/')
    team = models.CharField('група', max_length=10)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Гравець'
        verbose_name_plural = 'Гравці'


