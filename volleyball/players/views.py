from django.shortcuts import render
from .models import Player


def players(request):

    pl = Player.objects.all()
    return render(request, 'players/players.html', {'players': pl})
