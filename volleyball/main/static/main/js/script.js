var Cal = function(divId) {

    //save indicator div
    this.divId = divId;
  
    // day of week 
    this.DaysOfWeek = [
      'Пн',
      'Вт',
      'Ср',
      'Чт',
      'Пт',
      'Сб',
      'Нд'
    ];
  
    // month
    this.Months =['Січень', 'Лютий', 'Березень', 'Квітень', 'Травень', 'Черевень', 'Липень', 'Серпень', 'Вересень', 'Жовтень', 'Листопад', 'Грудень'];
  
    //get current date
    var d = new Date();
  
    this.currMonth = d.getMonth('9');
    this.currYear = d.getFullYear('22');
    this.currDay = d.getDate('3');
  };
  
  // move to next month
  Cal.prototype.nextMonth = function() {
    if ( this.currMonth == 11 ) {
      this.currMonth = 0;
      this.currYear = this.currYear + 1;
    }
    else {
      this.currMonth = this.currMonth + 1;
    }
    this.showcurr();
  };
  
  // move to previus month
  Cal.prototype.previousMonth = function() {
    if ( this.currMonth == 0 ) {
      this.currMonth = 11;
      this.currYear = this.currYear - 1;
    }
    else {
      this.currMonth = this.currMonth - 1;
    }
    this.showcurr();
  };
  
  // show current month
  Cal.prototype.showcurr = function() {
    this.showMonth(this.currYear, this.currMonth);
  };
  
  
  
  // show month (year, month)
  Cal.prototype.showMonth = function(y, m) {
  
    var d = new Date()
    // first day in select month
    , firstDayOfMonth = new Date(y, m, 7).getDay()
    // last day in select month
    , lastDateOfMonth =  new Date(y, m+1, 0).getDate()
    // first day in current month
    , lastDayOfLastMonth = m == 0 ? new Date(y-1, 11, 0).getDate() : new Date(y, m, 0).getDate();
  
  
    var html = '<table>';
  
    // mark select month and year
    html += '<thead><tr>';
    html += '<td colspan="7">' + this.Months[m] + ' ' + y + '</td>';
    html += '</tr></thead>';
  
  
    // title days of the week 
    html += '<tr class="days">';
    for(var i=0; i < this.DaysOfWeek.length;i++) {
      html += '<td>' + this.DaysOfWeek[i] + '</td>';
    }
    html += '</tr>';
  
    // white a day
    var i=1;
    do {
  
      var dow = new Date(y, m, i).getDay();
  
      // star new line in monday 
      if ( dow == 1 ) {
        html += '<tr>';
      }
      
      // if firs day not mon then show last days previus month
      else if ( i == 1 ) {
        html += '<tr>';
        var k = lastDayOfLastMonth - firstDayOfMonth+1;
        for(var j=0; j < firstDayOfMonth; j++) {
          html += '<td class="not-current">' + k + '</td>';
          k++;
        }
      }
  
      // white in current loop 
      var chk = new Date();
      var chkY = chk.getFullYear();
      var chkM = chk.getMonth();
      if (chkY == this.currYear && chkM == this.currMonth && i == this.currDay) {
        html += '<td class="today">' + i + '</td>';
      } else {
        html += '<td class="normal">' + i + '</td>';
      }
      // close line in sunday
      if ( dow == 0 ) {
        html += '</tr>';
      }
      // if last day in month not sunday show first day of next month
      else if ( i == lastDateOfMonth ) {
        var k=1;
        for(dow; dow < 7; dow++) {
          html += '<td class="not-current">' + k + '</td>';
          k++;
        }
      }
  
      i++;
    }while(i <= lastDateOfMonth);
  
    // end table
    html += '</table>';
  
    // mark HTML in div
    document.getElementById(this.divId).innerHTML = html;
  };
  
  // on load func
  window.onload = function() {
  
    // start app
    var c = new Cal("divCal");			
    c.showcurr();
  
    // mark btn
    getId('btnNext').onclick = function() {
      c.nextMonth();
    };
    getId('btnPrev').onclick = function() {
      c.previousMonth();
    };
  }
  
  
  function getId(id) {
    return document.getElementById(id);
  }




/*******************gallery*****************/