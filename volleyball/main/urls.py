from django.urls import path
from . import views


urlpatterns = [
    path('', views.index, name='home'),
    path('schedule', views.schedule, name='schedule'),
    path('gallery', views.gallery, name='gallery'),
    path('contacts', views.contacts, name='contacts')
]
