from django.shortcuts import render


def index(request):
    data = {
        'title': 'Ліцей №7',
        'values': ['some', 'Hello', 'Go'],
        'obj': {
            'car': 'audi',
            'age': 18,
            'likes': 'Football'
        }
    }
    return render(request, 'main/index.html', data)

def schedule(request):
    return render(request, 'main/schedule.html')


def gallery(request):
    return render(request, 'main/gallery.html')

def contacts(request):
    return render(request, 'main/contacts.html')
